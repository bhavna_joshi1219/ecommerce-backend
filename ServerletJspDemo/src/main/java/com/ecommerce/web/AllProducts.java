package com.ecommerce.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.products.Products;
import com.ecommerce.web.dao.ProductsDao;
import com.google.gson.Gson;

/**
 * Servlet implementation class AllProducts
 */
public class AllProducts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllProducts() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  response.addHeader("Access-Control-Allow-Origin", "*");
		    response.addHeader("Access-Control-Allow-Methods", "*");
		    response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/");
		  
		    
		// TODO Auto-generated method stub
    ProductsDao dao=new ProductsDao();
    List<Products> users=new ArrayList<>();
	    users=dao.getProducts1();
		Gson gson = new Gson();
		String userJSON;
		try {
			userJSON = gson.toJson(users);
			PrintWriter pw=response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			pw.write(userJSON);
			pw.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
