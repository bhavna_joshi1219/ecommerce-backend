package com.ecommerce.web.dao;
import com.ecommerce.products.Products;
import com.ecommerce.web.*;
import java.io.*;
import com.ecommerce.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ProductsDao {
	
	public List<Products> getProducts(int cate)
	{
		List<Products> users=new ArrayList<>();
		Products p = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/userdb", "root", "root");
			PreparedStatement ps= con.prepareStatement("SELECT * FROM product WHERE category_id=?");
			ps.setInt(1,cate);
			ResultSet rs = ps.executeQuery();
			System.out.println(rs.next());
			
			 while(rs.next()) {
				int id = rs.getInt("id");
				String name =rs.getString("name");
				String desc=rs.getString("product_desc");
				double price=rs.getDouble("price");
				int Qty=rs.getInt("quantity");
				int Category_id=rs.getInt("category_id");
				String image=rs.getString("image");
				p = new Products(id, name, price, Qty, desc,Category_id,image);
				users.add(p);
				
			}
		rs.close();
		ps.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return users;
		
		
		
	}

	public List<Products> getProducts1()
	{
		List<Products> users=new ArrayList<>();
		Products p = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/userdb", "root", "root");
			PreparedStatement stmt= con.prepareStatement("SELECT * FROM product");
			
			ResultSet rs = stmt.executeQuery();
			
			 while(rs.next()) {
				int id = rs.getInt("id");
				String name =rs.getString("name");
				String desc=rs.getString("product_desc");
				double price=rs.getDouble("price");
				int Qty=rs.getInt("quantity");
				int Category_id=rs.getInt("category_id");
				String image=rs.getString("image");
				p = new Products(id, name, price, Qty, desc,Category_id,image);
				users.add(p);
				
			}
		rs.close();
		stmt.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return users;
		
		
		
	}
	public int addInventory(Products prod) throws ClassNotFoundException {
	int record=0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/userdb", "root", "root");
			System.out.println(prod);
			PreparedStatement ps = con.prepareStatement("insert into product values(?,?,?,?,?,?,?)");
			ps.setInt(1,prod.getId());
			ps.setString(2, prod.getProductName());
			ps.setString(3, prod.getProdDesc());
			ps.setDouble(4, prod.getPrice());
			ps.setInt(5, prod.getQty());
			ps.setInt(6, prod.getCategory_id());
			ps.setString(7, prod.getImage());
			record=ps.executeUpdate();
			ps.close();
			return record;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return record;
}
//	public Products deleteProductUsingId(String prodId) throws ClassNotFoundException {
//		Products prod=null;
//		try {
//			Class.forName("com.mysql.cj.jdbc.Driver");
//			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/abdul", "root", "root");
//			
//				 PreparedStatement ps = con.prepareStatement("delete from products where prod_id=?");
//				 ps.setString(1, prodId);
//				 int rs=ps.executeUpdate();
//				 System.out.println("Row deleted sucessfully :"+rs);
//				 ps.close();
//				 } catch (SQLException e) {
//				 e.printStackTrace();
//				 }
//		return prod;
//	}
//	public int deleteProduct(int productId) throws ClassNotFoundException
//	{
//		int deleted=0;
//		try {
//			Class.forName("com.mysql.cj.jdbc.Driver");
//			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/abdul", "root", "root");
//			String sql = "delete from products where prod_Id = ?;";
//			PreparedStatement ps = con.prepareStatement(sql);
//			ps.setInt(1,productId);
//			ps.executeUpdate();
//			}catch(SQLException e) 
//		{e.printStackTrace();
//		}
//		return deleted;
//		}
	public int deleteProduct(int prod_id) throws ClassNotFoundException {
		int delete = 0;
		 try {
              Class.forName("com.mysql.cj.jdbc.Driver");
              Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/userdb", "root", "root");
              PreparedStatement ps = con.prepareStatement("delete from product where id=?");
              ps.setInt(1, prod_id);
              
              delete=ps.executeUpdate();
              System.out.println(delete);
              ps.close();
              return delete;
		 }catch(Exception e) {
			 e.printStackTrace();
		 	}
		 
		 return delete;
	}
	public int updateProductUsingId(int prodId, double price,String name,int qty,String prod_desc, String image) 
	{
		int rs2 =0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/userdb", "root", "root");
			
			PreparedStatement ps2 = con.prepareStatement("UPDATE product SET name=?,product_desc=?, price = ?,quantity=?, image=? WHERE id=?");
			ps2.setString(1, name);
			ps2.setString(2, prod_desc);
			ps2.setDouble(3, price);
			ps2.setInt(4, qty);
			ps2.setString(5, image);
			ps2.setInt(6, prodId);
			rs2=ps2.executeUpdate();
		 System.out.println("The  product price has updated sucessfully"+rs2);
				 ps2.close();
				 } catch (SQLException e) {
				 e.printStackTrace();
				 }
		catch (ClassNotFoundException e) {
			 e.printStackTrace();
			 
	}
	return rs2;
	}
}
