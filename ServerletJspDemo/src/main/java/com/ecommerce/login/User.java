package com.ecommerce.login;

public class User {
	
	int user_id;
	String username;
	String password;
	String email_id;
	String address;
	String user_role;


    
 

	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(int user_id,String username, String password, String email_id, String address, String user_role) {
		super();
	    this.user_id=user_id;
		this.username = username;
		this.password = password;
		this.email_id = email_id;
		this.address = address;
		this.user_role = user_role;
	}
	
	public User(String username, String password, String email_id) {
		super();
		this.username = username;
		this.password = password;
		this.email_id = email_id;
	}
	
	

	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}

	public String getAddress() {
		// TODO Auto-generated method stub
		return address;
	}


	public String getUser_role() {
		// TODO Auto-generated method stub
		return user_role;
	}

	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", username=" + username + ", password=" + password + ", email_id="
				+ email_id + ", address=" + address + ", user_role=" + user_role + "]";
	}

	
	


}
